package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn) {
        HashMap<String, Double> pricetable= new HashMap<>();
        pricetable.put("1", 10.0);
        pricetable.put("2", 45.0);
        pricetable.put("3", 20.0);
        pricetable.put("4", 35.0);
        pricetable.put("5", 50.0);
        return pricetable.get(isbn);
    }
}
